import {Component, OnInit} from '@angular/core';
import { navItems } from '../../_nav';

import { UserAuthService } from '../../shared/user-auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  public iduser = atob(localStorage.getItem('id'));
  public user: any;
  public image: any;

  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(private userAuthService: UserAuthService) { }

  ngOnInit(): void {

    this.userAuthService.getUsertAuth(this.iduser).subscribe(res =>{
      this.user = res;
      this.user = this.user[0];
    });
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
}
