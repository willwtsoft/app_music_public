<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlbumsController extends Controller
{
    public function selectAlbums()
    {
        $albums = DB::table('albums')->get();
        print(json_encode($albums));
    }

    public function insertAlbum(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        DB::table('albums')->insertGetId($data);
    }

    public function updateAlbum(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $idalbuns = $data["idalbuns"];
       
        DB::table('albums')->where('idalbuns', $idalbuns)->update($data);        
    }   

    public function deleteAlbum(Request $request)
    {
        $idalbuns = json_decode($request->getContent(), true);

        DB::table('albums')->where('idalbuns', $idalbuns)->delete();
    }   

    public function searchAlbums(Request $request)
    {
        $searchByArtist = $request->getContent();                
        
        $results = DB::table('albums')->where('artist', 'like', '%' .$searchByArtist. '%')->get();
        
        print(json_encode($results));
    }
}
