import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecordUsersService {

  private header: any = {headers: 
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'    
  })}

  constructor(private http: HttpClient) { }

  selectRole()
  {
    return this.http.get(`${environment.apiUrl}/selectRole`, this.header);
  }

  saveRecordUser(data: any)
  {
    return this.http.post(`${environment.apiUrl}/saveRecordUser`, data, this.header);
  }
  
}
