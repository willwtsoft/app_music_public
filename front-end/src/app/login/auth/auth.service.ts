import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private header: any = {headers:
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'
  })}

  constructor(private http: HttpClient) { }

  login(credentials: {username: string, password: string})
  {
    return this.http.post(`${environment.apiUrl}/login`, credentials, this.header);
  }

  recoverPassword(data: string)
  {
    return this.http.post(`${environment.apiUrl}/recoverPassword`, data, this.header);
  }

  codeVerify(data: any)
  {
    return this.http.post(`${environment.apiUrl}/codeVerify`, data, this.header);
  }


  updatePassword(data: any)
  {
    data.iduser = localStorage.getItem('idrecoverpassword');

    return this.http.post(`${environment.apiUrl}/updatePassword`, data, this.header);
  }

}
