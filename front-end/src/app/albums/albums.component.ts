import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthService } from '../shared/user-auth.service';
import { AlbumsService } from './albums.service';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalService } from './../modal/modal.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {


  public listAlbums: any;
  public idusers = atob(localStorage.getItem('id'));
  public user: any;
  public idalbuns: number;
  public album_descrition_modal :string;

  searchByArtist = new FormControl();

  modalRef?: BsModalRef;
  message?: string;

  constructor(
    private albumsService: AlbumsService,
    private userAuthService: UserAuthService,
    private router: Router,
    private bsModalService: BsModalService,
    private modalService: ModalService,

    ) { }

  ngOnInit(): void {

    this.getUsertAuth();
    this.selectAlbums();
  }

  selectAlbums()
  {
    this.albumsService.selectAlbums().subscribe(res => {
      this.listAlbums = res;
    });
  }

  // verify if object is empty
  verifyObect(obj: any)
  {
    for(var prop in obj)
    {
      if(obj.hasOwnProperty(prop))
      {
        return true;
      }
    }
    return false;
  }

  searchAlbums()
  {
    let searchByArtist = this.searchByArtist.value;

    this.albumsService.searchAlbums(searchByArtist).subscribe(res => {
     this.listAlbums = res;

     // if theres not albums with the name filled will list all albums
     if (!this.verifyObect(this.listAlbums))
     {
        this.selectAlbums();
     }
    });
  }

  loadFormAlbum(album: any)
  {
    this.router.navigate(['/albums/form-albums', album]);
  }

  getUsertAuth()
  {
    this.userAuthService.getUsertAuth(this.idusers).subscribe(res =>{
      this.user = res;
      this.user = this.user[0];
    });
  }

  openModal(template: TemplateRef<any>, album: any) {
    this.modalRef = this.bsModalService.show(template, {class: 'modal-sm'});
    this.idalbuns = album.idalbuns;
    this.album_descrition_modal = album.name_album;
  }

  confirm(): void {
    this.message = 'Confirmed!';
    this.modalRef?.hide();

    this.albumsService.deleteAlbum(this.idalbuns).subscribe(res =>{

      this.selectAlbums();
      this.modalService.showModal("Album Deleted successfully!");

    });
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef?.hide();
  }

}
