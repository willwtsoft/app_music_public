import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RecordUsersService } from './record-users.service';
import { ModalService } from './../modal/modal.service';

@Component({
  selector: 'app-record-users',
  templateUrl: './record-users.component.html',
  styleUrls: ['./record-users.component.scss']
})
export class RecordUsersComponent implements OnInit {

  f: FormGroup;
  public  user: any;
  public  resp: any;
  public  listRoles: any;
  public  verifyEqualsPass: boolean;
  public  hideFormPersonalData: boolean;
  

  constructor(
    private recordUsersService: RecordUsersService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: ModalService
    ) { }

  ngOnInit(): void
  {
    this.f = this.formBuilder.group({
      idrole: [null, [Validators.required]],
      full_name: [null, [Validators.required, Validators.minLength(6)]],
      username: [null, [Validators.required, Validators.minLength(8)]],
      
      password: [null, [Validators.required, Validators.minLength(8)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(8)]],
    });
        
    this.verifyEqualsPass = false;    
    this.user = {};
    this.selectRole();
  }

  selectRole()
  {
    this.recordUsersService.selectRole().subscribe(res =>{
      this.listRoles = res;
    }, 
    error => 
    {
        console.log(JSON.stringify(error));  
    });
  }

  saveRecordUser()
  { 
    this.user = this.f.value;    
    
    this.recordUsersService.saveRecordUser(this.user).subscribe(res =>{
      
      this.resp = res;
      
        /*valid the user*/
      if (this.resp == "User already exists!")
      {
        this.modalService.showModal(this.resp);
      }
      else
      {
        
        this.modalService.showModal("Cadastro Realizado");
        this.router.navigate(['']);
      }
    }, 
    error => 
    {
        console.log(JSON.stringify(error));  
    });
    
    
  }

  changeConfirmPassword(e: any)
  {
    const password = this.f.get('password').value;
    const confirmPassword = this.f.get('confirmPassword').value;
    
    if (password === confirmPassword) 
    {        
        this.verifyEqualsPass = true;
    } else 
    {
        this.verifyEqualsPass = false;
    }
  }  

  showFormPersonalData()
  {    
      this.hideFormPersonalData = false;
  }

}
