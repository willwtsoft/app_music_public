import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthService } from '../shared/user-auth.service';
import { ArtistsService } from './artists.service';


@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {

  public iduser = atob(localStorage.getItem('id'));
  public listArtists: any;

  constructor(
    private router: Router,
    private artistsService: ArtistsService,
    private userAuthService: UserAuthService
  ) { }

  ngOnInit(): void {

    this.getListArtists();
  }

  getListArtists()
  {
    this.artistsService.getListArtists().subscribe(res => {
        this.listArtists = res;
        /*como o objeto veio um array dentro do outro precisei desse Json.parse*/
        this.listArtists = JSON.parse(this.listArtists);

    })
  }

  openFormAlbum(artists: any)
  {
    // add artist as key to use in form-albums
    artists.artist = artists.name;
    this.router.navigate(['/albums/form-albums', artists]);
  }
}
