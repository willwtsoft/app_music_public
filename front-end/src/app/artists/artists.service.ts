import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ArtistsService {

  private urlMoatBuilders = "https://moat.ai/api/task/";

  private header: any = {headers:
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'
  })}

  private headerMoat: any = {headers: new HttpHeaders({'Basic': 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='})}

  constructor(private http: HttpClient) { }

  getListArtists()
  {
    // aqui eu tentei fazer a requisição direto do angular na api da moat builder e da erro de CORS
    //return this.http.get(this.urlMoatBuilders, this.headerMoat);

    // aqui estou mandando para o meu back end para tentar fazer o request na api pelo guzzle
    return this.http.get(`${environment.apiUrl}/getListArtists`, this.header);
  }

}
