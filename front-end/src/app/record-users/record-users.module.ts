import { RecordUsersService } from './record-users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ImageCropperModule } from 'ngx-image-cropper';

import { NgxMaskModule, IConfig } from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

import { RecordUsersComponent } from './record-users.component';

@NgModule({
  declarations: [RecordUsersComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    ImageCropperModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [
    RecordUsersService,
    FormBuilder      
  ],
})
export class RecordUsersModule { }
