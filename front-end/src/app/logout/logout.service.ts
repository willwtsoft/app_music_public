import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  private header: any = {headers:
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'
  })}

  constructor(private http: HttpClient) { }

  logout()
  {
    return this.http.get(`${environment.apiUrl}/logout`, this.header);
  }
}
