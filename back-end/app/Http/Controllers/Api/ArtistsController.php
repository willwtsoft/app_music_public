<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class ArtistsController extends Controller
{
    public function getListArtists()
    {
        /* Instancia a classe Client do Http Client Guzzle*/
        $client = new Client();
        
        $res = $client->post('https://moat.ai/api/task/', [
            "headers"=>[
                        "Basic"=>"ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=="
                    ]
        ]);

        $res = $res->getBody()->getContents();
        print(json_encode($res));
    }

    private function getHeader()
    {
        return [
                "Basic" => "ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=="                
            ];
    }
}
