import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';

import { AuthService } from './../auth/auth.service';
import { ModalService } from './../../modal/modal.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  f: FormGroup;
  public mensagem: string;
  public resp: any;
  public verifyEqualsPass: boolean;
  
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private modalService: ModalService
  )  {  }

  ngOnInit(): void 
  {
    this.f = this.formBuilder.group({
      iduser:  [null],
      newPassword: [null, [Validators.required, Validators.minLength(8)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(8)]],
    });

    this.verifyEqualsPass = false;

  }

  changeConfirmPassword(e: any)
  {
    const newPassword = this.f.get('newPassword').value;
    const confirmPassword = this.f.get('confirmPassword').value;
    
    if (newPassword === confirmPassword) 
    {        
        this.verifyEqualsPass = true;
    } else 
    {
        this.verifyEqualsPass = false;
    }
  }
  
  updatePassword()
  {        
   
    this.authService.updatePassword(this.f.value).subscribe(res =>{
            
      this.mensagem = "Senha altarada!";
      this.modalService.showModal(this.mensagem);
      this.router.navigate(['']);
            
    });
  }

}
