-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 03-Jan-2022 às 21:22
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `app_musica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `albums`
--

CREATE TABLE `albums` (
  `idalbuns` int(11) NOT NULL,
  `idusers` int(11) NOT NULL,
  `artist` varchar(255) NOT NULL,
  `name_album` varchar(255) NOT NULL,
  `year_album` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `albums`
--

INSERT INTO `albums` (`idalbuns`, `idusers`, `artist`, `name_album`, `year_album`) VALUES
(1, 5, 'Lady Gaga', 'Chromatica', 2020),
(3, 5, 'Shakira', 'Pies descalzos', 2006),
(4, 5, 'Britney Spears', 'In the Zone', 2003),
(7, 4, 'Lady Gaga', 'Born This Way', 2011),
(8, 4, 'Taylor Swift', 'Fearless', 2008),
(9, 5, 'Justin Bieber', 'My World', 2010);

-- --------------------------------------------------------

--
-- Estrutura da tabela `role`
--

CREATE TABLE `role` (
  `idrole` int(11) NOT NULL,
  `description_role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `role`
--

INSERT INTO `role` (`idrole`, `description_role`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `idusers` int(11) NOT NULL,
  `idrole` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`idusers`, `idrole`, `full_name`, `username`, `password`) VALUES
(2, 2, 'Natalia Karine', 'naty2022', '$2a$08$MTE5NzAxMTc2NTYxY2Y1Men3pNS3gUXhGOHeT37.T5quDDG3KaL1i'),
(3, 2, 'Ana Monteiro', 'ana12345', '$2a$08$MTQwMDAzMTQ1NjFjZjUzNeSQz3HgLY4cSU8qgNrVUIYdtDhPp6LOG'),
(4, 1, 'Willian dos Reis Havy Metal', 'willrock', '$2a$08$ODA4MTY1NjAxNjFjZjYwO.PRmHhdSF7E6LN7j4EEA2tvP3Yy4Ykye'),
(5, 2, 'Suellen Faria', 'suellen2022', '$2a$08$MTU4MDkxMTY1NjYxY2Y2M.kOAS4bSpUHsqT2WUPDUQVqlA90lv.Pa');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`idalbuns`),
  ADD KEY `fk_process_users1_idx` (`idusers`);

--
-- Índices para tabela `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`idrole`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`),
  ADD KEY `fk_users_type_users_idx` (`idrole`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `albums`
--
ALTER TABLE `albums`
  MODIFY `idalbuns` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `role`
--
ALTER TABLE `role`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `fk_process_users1` FOREIGN KEY (`idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_type_users` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
