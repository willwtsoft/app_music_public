import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumsService } from '../albums.service';

import { ModalService } from './../../modal/modal.service';

@Component({
  selector: 'app-form-albums',
  templateUrl: './form-albums.component.html',
  styleUrls: ['./form-albums.component.scss']
})
export class FormAlbumsComponent implements OnInit {

  public idusers = atob(localStorage.getItem('id'));
  public idalbuns :number;

  f: FormGroup;
  public artists :string;
  public album: any;
  public response: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private routeActivated: ActivatedRoute,
    private albumsService: AlbumsService,
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    this.f = this.formBuilder.group({
      idalbuns:   [],
      name_album: [null, [Validators.required, Validators.minLength(2)]],
      year_album: [null, [Validators.required, Validators.minLength(4)]],
    });

    this.routeActivated.params.subscribe(
      (params: any)=> {

        // load name artist to artists to show in header from form
        this.artists = params["artist"];

        // full the form-albums when the user is editing
        this.f.patchValue({
          idalbuns:    params["idalbuns"],
          name_album:  params["name_album"],
          year_album:  params["year_album"]
        });

      }
    );
  }

  insertUpdateAlbum()
  {
    this.album = this.f.value;

    this.album.idusers = this.idusers;

    // if the input idalbuns be different of undefined is update
    if (this.album.idalbuns != undefined)
    {
      this.albumsService.updateAlbum(this.album).subscribe(res =>{

        this.modalService.showModal("Album update successfully!");

      });
    }
    else
    {
      // How is new album, put the name artists
      this.album.artist = this.artists;

      this.albumsService.insertAlbum(this.album).subscribe(res =>{

        this.modalService.showModal("Album inserted successfully!");

      });
    }

    this.router.navigate(['albums']);
  }

}
