import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{

  constructor(
    private router: Router,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean
  {
      let userToken = localStorage.getItem('token');

      /* verifica se tem token de login válido, se tiver retorna o token
      dessa forma retornamos true para o guarda de rotas, se não tiver
      token de login ativo retorna null, então retornamos false para o
      usuário não acessar a rota com a guarda*/
      if (userToken != null)
      {
        return true;
      }

      return false;
  }

}
