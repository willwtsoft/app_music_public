import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsComponent } from './albums.component';
import { FormAlbumsComponent } from './form-albums/form-albums.component';

const routes: Routes = [
  {
    path: '',
    component: AlbumsComponent,
  },
  {
    path: 'form-albums',
    component: FormAlbumsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumsRoutingModule {}
