import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { ModalService } from './../../modal/modal.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  f: FormGroup;
  public login: any;

  public hideFormLogin: boolean;
  public mensagem: string;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private modalService: ModalService
  ) { }

  ngOnInit(): void
  {
    this.f = this.formBuilder.group({
      username: [null, [Validators.required, Validators.minLength(8)]],
      password: [null, [Validators.required, Validators.minLength(8)]],
    });
  }

  onSubmit()
  {
    this.authService.login(this.f.value).subscribe(
    res =>
    {

      this.login = res;
      localStorage.setItem('token', this.login.token);
      localStorage.setItem('id', btoa(this.login.user.idusers));

      this.router.navigate(['/artists']);
    },
    error =>
    {
        console.log(error);
        this.modalService.showModal(error.error.error);
    });
  }

  openFormRecordUsers()
  {
    this.router.navigate(['/record-users']);
  }

  backToLogin()
  {
     this.hideFormLogin = false;
  }

}
