<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Api\BcryptController;

class RecordUserController extends Controller
{
    public function selectRole()
    {
        $roles = DB::table('role')->get();
        print(json_encode($roles));
    }

    public function saveRecordUser(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $respValidUserName = $this->validUserName($data["username"]);

        if(!empty($respValidUserName))
        {
            print(json_encode('User already exists!')); die();
        }
               
        unset($data["confirmPassword"]);  
        
        $data["password"] = BcryptController::hash($data["password"]);
        
        DB::table('users')->insertGetId($data);
        
    }

    private function validUserName($username)
    {
        $sql = DB::select("SELECT * FROM users 
                WHERE username = '{$username}'"); 

        return $sql;
    }  
   
}
