import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlbumsService } from './albums.service';
import { AlbumsComponent } from './albums.component';
import { FormAlbumsComponent } from './form-albums/form-albums.component';
import { AlbumsRoutingModule } from './albums.routing.module';

import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AlbumsComponent, FormAlbumsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AlbumsRoutingModule
  ],
  providers: [
    AlbumsService,
    FormBuilder
  ],
})
export class AlbumsModule { }
