<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


use Tymon\JWTAuth\JWTAuth;

use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    private $jwtAuth;

    public function __construct(JWTAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        //return response()->json($credentials);
        
        if (!$token = $this->jwtAuth->attempt($credentials)) {
            return response()->json(['error' => 'User or Password Wrong'], 401);
        }

        $user = $this->jwtAuth->User();

        return response()->json(compact('token', 'user'));
    }

    public function refresh()
    {
        $token = $this->jwtAuth->getToken();
        $token = $this->jwtAuth->refresh($token);

        return response()->json(compact('token'));
    }

    public function logout()
    {
        $token = $this->jwtAuth->getToken();
        $this->jwtAuth->invalidate($token);

        return response()->json(['logout']);
    }

    public function me()
    {
        if (!$user = $this->jwtAuth->parseToken()->authenticate()) {
            return response()->json(['error' => 'user_not_found'], 404);
        }

        return response()->json(compact('user'));
    }

    public function getUsertAuth(Request $request)
    {
        $id = json_decode($request->getContent(), true);

        $sql = DB::select("SELECT * FROM users 
        WHERE idusers = '{$id}'"); 

        return $sql;
    }
}