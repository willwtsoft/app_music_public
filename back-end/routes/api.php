<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\AuthController@login');
Route::post('refresh', 'Api\AuthController@refresh');
Route::get('logout', 'Api\AuthController@logout');

Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Api\\'], 
function()
{
    Route::post('me', 'AuthController@me');
        
});

Route::group(['namespace' => 'Api\\'], 
function()
{        
    Route::post('saveRecordUser', 'RecordUserController@saveRecordUser');

    Route::post('getUsertAuth', 'AuthController@getUsertAuth');

    Route::get('selectRole', 'RecordUserController@selectRole');    

    Route::get('getListArtists', 'ArtistsController@getListArtists');    
    
    Route::get('selectAlbums', 'AlbumsController@selectAlbums');
    
    Route::post('insertAlbum', 'AlbumsController@insertAlbum');
    
    Route::put('updateAlbum', 'AlbumsController@updateAlbum');

    Route::post('deleteAlbum', 'AlbumsController@deleteAlbum');

    Route::post('searchAlbums', 'AlbumsController@searchAlbums');
    
});


