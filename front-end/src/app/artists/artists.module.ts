import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistsService } from './artists.service';
import { ArtistsComponent } from './artists.component';
import { ArtistsRoutingModule } from './artists.routing.module';

import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';



@NgModule({
  declarations: [ArtistsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    ArtistsRoutingModule,
  ],
  providers: [
    ArtistsService,
    FormBuilder
  ],
})
export class ArtistsModule { }
