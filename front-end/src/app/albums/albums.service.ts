import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  private header: any = {headers:
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'
  })}

  constructor(private http: HttpClient) { }

  selectAlbums()
  {
    return this.http.get(`${environment.apiUrl}/selectAlbums`, this.header);
  }

  searchAlbums(searchByArtist: string)
  {
    return this.http.post(`${environment.apiUrl}/searchAlbums`, searchByArtist, this.header);
  }

  insertAlbum(new_album: any)
  {
    return this.http.post(`${environment.apiUrl}/insertAlbum`, new_album, this.header);
  }

  updateAlbum(album: any)
  {
    return this.http.put(`${environment.apiUrl}/updateAlbum`, album, this.header);
  }

  deleteAlbum(idalbuns: number)
  {
    return this.http.post(`${environment.apiUrl}/deleteAlbum`, idalbuns, this.header);
  }

}
