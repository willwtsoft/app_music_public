import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';;
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {

  private header: any = {headers: 
    new HttpHeaders({'Accept': 'application/json',
     'Content-Type': 'application/json'    
  })}

  constructor(private http: HttpClient) { }

  getUsertAuth(id: any)
  {
    return this.http.post(`${environment.apiUrl}/getUsertAuth`, id, this.header);
  }
}
